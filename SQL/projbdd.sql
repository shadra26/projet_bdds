DROP TABLE IF EXISTS coach cascade ;    -- 1--
DROP TABLE IF EXISTS player cascade;       -- 2 --
DROP TABLE IF EXISTS teammates cascade;      -- 3 --
DROP TABLE IF EXISTS teams cascade;    -- 4 --
DROP TABLE IF EXISTS played_against cascade;        -- 5 --
DROP TABLE IF EXISTS playes_for cascade;   -- 6 --
DROP TABLE IF EXISTS against_team cascade ;    -- 7 --



CREATE TABLE coach(
     coach_id serial PRIMARY KEY,
     name varchar(50) NOT NULL 
);


CREATE TABLE player(
     player_id serial PRIMARY KEY,
     name varchar(50) NOT NULL,
     age INTEGER NOT NULL CHECK (age < 40 and age>=0) ,
     number INTEGER NOT NULL CHECK (number >= 0),
     height INTEGER NOT NULL CHECK( height >= 0),
     weight FLOAT NOT NULL CHECK( weight >= 0),
     coach_id INTEGER,
     FOREIGN KEY(coach_id) REFERENCES coach(coach_id)
);



CREATE TABLE teammates(
     player_id INTEGER,
     teammate_id INTEGER,
     
     FOREIGN KEY(player_id) REFERENCES player(player_id),
     FOREIGN KEY(teammate_id) REFERENCES player(player_id),
     PRIMARY KEY(player_id,teammate_id)
);


CREATE TABLE teams(
     team_id serial PRIMARY KEY,
     name varchar(50) NOT NULL,
     surname varchar(50),
     coach_id INTEGER,
     FOREIGN KEY(coach_id) REFERENCES coach(coach_id)
);


CREATE TABLE played_against(
     player_id  INTEGER,
     team_id INTEGER,
     minutes INTEGER NOT NULL CHECK (minutes >= 0 and minutes< 90) ,
     points INTEGER NOT NULL,
     assists INTEGER,
     rebounds INTEGER,
     turnover INTEGER,
     date DATE,
     FOREIGN KEY(player_id) REFERENCES player(player_id),
     FOREIGN KEY(team_id) REFERENCES teams(team_id),
     PRIMARY KEY (player_id,team_id,date)
);


CREATE TABLE playes_for(
     player_id  INTEGER,
     team_id INTEGER,
     salary INTEGER NOT NULL CHECK(salary >=0),
     FOREIGN KEY(player_id) REFERENCES player(player_id),
     FOREIGN KEY(team_id) REFERENCES teams(team_id),
     PRIMARY KEY (player_id,team_id)
);


CREATE TABLE against_team(
     team1_id  INTEGER,
     team2_id INTEGER,
     won INTEGER,
     date DATE,
     FOREIGN KEY(team1_id) REFERENCES teams(team_id),
     FOREIGN KEY(team2_id) REFERENCES teams(team_id),
     PRIMARY KEY (team1_id,team2_id,date)
);
 
CREATE INDEX id_player 
ON player(player_id);

CREATE INDEX id_team 
ON teams(team_id);

CREATE INDEX id_coach
ON teams(coach_id);

CREATE INDEX againstplayer
ON played_against(player_id);


CREATE INDEX playes_f
ON playes_for(player_id);

CREATE INDEX playes_te_f
ON playes_for(team_id);

CREATE INDEX against_t1
ON against_team(team1_id);

CREATE INDEX against_t2
ON against_team(team2_id);



\copy coach(coach_id,name) FROM coach.csv WITH (FORMAT csv, HEADER, DELIMITER ';');

\copy player(player_id,name,age,number,height,weight,coach_id) FROM player.csv WITH (FORMAT csv, HEADER,DELIMITER ';');

\copy teammates(player_id,teammate_id) FROM teammates.csv WITH (FORMAT csv, HEADER,DELIMITER ';');

\copy teams(team_id,name,surname,coach_id) FROM Teams.csv WITH (FORMAT csv, HEADER);

\copy played_against(player_id,team_id,minutes,points,assists,rebounds,turnover, date) FROM played_against.csv WITH (FORMAT csv, HEADER,DELIMITER ';'); 

\copy playes_for(player_id,team_id,salary) FROM playes_for.csv WITH (FORMAT csv, HEADER);

\copy against_team(team1_id,team2_id,won,date) FROM against_team.csv WITH (FORMAT csv, HEADER);






SELECT p.name
FROM played_against pa JOIN player p
ON p.player_id = pa.player_id
WHERE pa.team_id IN (SELECT team_id FROM teams WHERE name IN ('Memphis Grizzlies', 'Brooklyn Nets', 'Philadelphia 76ers'))
GROUP BY p.name
HAVING COUNT(DISTINCT pa.team_id) = 3;

SELECT p.name AS player, array_agg(DISTINCT t.name) AS teams
FROM played_against pa
JOIN player p ON pa.player_id = p.player_id
JOIN teams t ON pa.team_id = t.team_id
GROUP BY p.name;


SELECT t.name AS team, array_agg(p.name) AS players
FROM playes_for pf
JOIN teams t ON pf.team_id = t.team_id
JOIN player p ON pf.player_id = p.player_id
GROUP BY t.name;


SELECT t.name AS team, array_agg(p.name) AS players
FROM teammates pf
JOIN player t ON pf.player_id = t.player_id
JOIN player p ON pf.teammate_id = p.player_id
GROUP BY t.name;


SELECT foo.name , count(foo.name) FROM 
((SELECT t1.name as name FROM teams t1
JOIN against_team m ON m.team1_id = t1.team_id AND m.won = m.team1_id)
UNION ALL
(SELECT t1.name as name FROM teams t1
JOIN against_team m2 ON m2.team2_id = t1.team_id AND m2.won = m2.team2_id ) 
) as foo
GROUP BY foo.name;



SELECT c.name, COUNT(r) AS playersCoached
FROM coach c
LEFT JOIN player r ON c.coach_id = r.coach_id
GROUP BY c.name;


SELECT coach.name AS coach,array_agg(p.name) AS Players
FROM player p
JOIN coach ON p.coach_id = coach.coach_id
WHERE p.name IN (select name from player)
GROUP BY coach.name ;


SELECT t2.name
FROM teams t1
JOIN teams t2 ON t1.team_id<> t2.team_id
LEFT JOIN against_team at ON t1.team_id =at.team2_id AND t2.team_id = at.team1_id
WHERE t1.name = 'LA Lakers' AND at.team1_id IS NULL;



SELECT team2.name
FROM teams team1, teams team2
WHERE team1.name = 'LA Lakers'
AND NOT EXISTS (SELECT 1 FROM against_team WHERE (team1.team_id = team1_id AND team2.team_id = team2_id) OR (team1.team_id = team2_id AND team2.team_id = team1_id))
AND team1.name <> team2.name;


SELECT name FROM (
SELECT teammate.name
FROM Player lebron
JOIN teammates ON teammates.player_id = lebron.player_id
JOIN Player teammate ON teammate.player_id = teammates.teammate_id
WHERE lebron.name = 'LeBron James'
UNION
SELECT p.name
FROM Player p
JOIN Coach c ON c.coach_id = p.coach_id
JOIN Player lebron ON lebron.coach_id = p.coach_id
WHERE lebron.name = 'LeBron James' AND p.name <> 'LeBron James'
) AS player_name;


(SELECT p.name, p.age
FROM player p
WHERE p.age IS NOT NULL
ORDER BY p.age ASC
LIMIT 1)
UNION
(SELECT p.name, p.age
FROM player p
WHERE p.age IS NOT NULL
ORDER BY p.age DESC
LIMIT 1);


WITH RECURSIVE rec_teams AS (
SELECT team_id AS id, name
FROM teams
WHERE name = 'LA Lakers'
UNION
SELECT t.team_id, t.name
FROM rec_teams rt
JOIN against_team at ON at.team1_id = rt.id OR at.team2_id = rt.id
JOIN teams t ON t.team_id = at.team1_id OR t.team_id = at.team2_id
WHERE t.team_id <> rt.id )
SELECT name FROM rec_teams where name NOT LIKE '%LA Lakers%';
